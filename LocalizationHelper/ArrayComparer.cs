﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Strut {
	class ArrayComparer<T> : IEqualityComparer<T[]> {
		public bool Equals(T[] x, T[] y) {
			if (x.Length != y.Length)
				return false;
			for (int i = 0; i < x.Length; ++i) {
				if (!Equals(x[i], y[i]))
					return false;
			}
			return true;
		}

		public int GetHashCode(T[] obj) {
			int result = 17;
			for (int i = 0; i < obj.Length; i++) {
				result = result * 23 + obj[i].GetHashCode();
			}
			return result;
		}
	}
}
