﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Strut {
	class ColorFormatter : IColorFormattingHandler {
		private static Dictionary<char, (ConsoleColor fg, ConsoleColor bg)> FormattingCodes { get; } = new() {
			['0'] = (ConsoleColor.Gray, ConsoleColor.Black),
			['1'] = (ConsoleColor.White, ConsoleColor.DarkBlue),
			['2'] = (ConsoleColor.White, ConsoleColor.DarkGreen),
			['3'] = (ConsoleColor.White, ConsoleColor.DarkCyan),
			['4'] = (ConsoleColor.White, ConsoleColor.DarkRed),
			['5'] = (ConsoleColor.White, ConsoleColor.DarkMagenta),
			['6'] = (ConsoleColor.White, ConsoleColor.DarkYellow),
			['7'] = (ConsoleColor.Black, ConsoleColor.Gray),
			['8'] = (ConsoleColor.White, ConsoleColor.DarkGray),
			['9'] = (ConsoleColor.Black, ConsoleColor.Blue),
			['a'] = (ConsoleColor.Black, ConsoleColor.Green),
			['b'] = (ConsoleColor.Black, ConsoleColor.Cyan),
			['c'] = (ConsoleColor.Black, ConsoleColor.Red),
			['d'] = (ConsoleColor.White, ConsoleColor.Magenta),
			['e'] = (ConsoleColor.Black, ConsoleColor.Yellow),
			['f'] = (ConsoleColor.DarkGray, ConsoleColor.White),
			['k'] = (ConsoleColor.DarkRed, ConsoleColor.White),
			['l'] = (ConsoleColor.DarkRed, ConsoleColor.White),
			['m'] = (ConsoleColor.DarkRed, ConsoleColor.White),
			['n'] = (ConsoleColor.DarkRed, ConsoleColor.White),
			['o'] = (ConsoleColor.DarkRed, ConsoleColor.White),
			['r'] = (ConsoleColor.DarkRed, ConsoleColor.White)
		};
		private static Regex FormattingCodesPattern { get; } = new Regex($"[&§][{String.Join("", FormattingCodes.Keys)}]");

		public ConsoleColor Foreground { get; set; } = ColorFormattedString.DefaultForeground;
		public ConsoleColor Background { get; set; } = ColorFormattedString.DefaultBackground;
		public bool HighlightPlaceholders { get; set; } = true;
		public bool HighlightEscapedCharacters { get; set; } = true;
		public bool HighlightFormattingCodes { get; set; } = true;
		public bool HighlightLeadingAndTrailingWhitespace { get; set; } = true;
		public bool HighlightAllWhitespace { get; set; } = false;

		public ColorFormattedString GetColorFormattedString(string str) => ColorFormatString(
				str,
				Foreground,
				Background,
				HighlightPlaceholders,
				HighlightEscapedCharacters,
				HighlightFormattingCodes,
				HighlightLeadingAndTrailingWhitespace,
				HighlightAllWhitespace
			);

		public static ColorFormattedString ColorFormatString(
			string str,
			ConsoleColor defaultForeground = ColorFormattedString.DefaultForeground,
			ConsoleColor defaultBackground = ColorFormattedString.DefaultBackground,
			bool highlightPlaceholders = true,
			bool highlightEscapedCharacters = true,
			bool highlightFormattingCodes = true,
			bool highlightLeadingAndTrailingWhitespace = true,
			bool highlightAllWhitespace = false
		) {
			var list = new List<ColorFormattedStringComponent>();
			var placeholders = PlaceholderHandler.Pattern.Matches(str).ToDictionary(m => m.Index, m => m.Length);
			var escapes = StringEscaper.GetEscapeIndexes(str).ToDictionary(i => i, _ => 2);
			var formattingCodes = FormattingCodesPattern.Matches(str).ToDictionary(m => m.Index, m => m.Length);
			var whitespace = Regex.Matches(str, @"\s+").ToDictionary(m => m.Index, m => m.Length);

			string stripped;
			{
				IEnumerable<KeyValuePair<int, int>> dict = new List<KeyValuePair<int, int>>();
				if (highlightEscapedCharacters) {
					dict = dict.Concat(escapes);
				}
				if (highlightFormattingCodes) {
					dict = dict.Concat(formattingCodes);
				}
				stripped = StripInternal(str, dict, true);
			}

			int leadingWsIndex = stripped.CountLeadingWhitespace();
			int trailingWsIndex = stripped.Length - (String.IsNullOrWhiteSpace(stripped) ? 0 : stripped.CountTrailingWhitespace()) - 1;

			var builder = new StringBuilder();
			for (int i = 0; i < str.Length;) {
				if (highlightPlaceholders && placeholders.TryGetValue(i, out int phVal)) {
					DumpBuilder();
					list.Add(new ColorFormattedStringComponent(str.Substring(i, phVal), ConsoleColor.Blue));
					i += phVal;
				} else if (highlightEscapedCharacters && escapes.TryGetValue(i, out int escVal)) {
					DumpBuilder();
					list.Add(new ColorFormattedStringComponent(str.Substring(i, 1), ConsoleColor.DarkYellow, ConsoleColor.Yellow));
					list.Add(new ColorFormattedStringComponent(str.Substring(i + 1, escVal - 1), ConsoleColor.Red, ConsoleColor.Yellow));
					i += escVal;
				} else if (highlightFormattingCodes && formattingCodes.TryGetValue(i, out int fcVal)) {
					DumpBuilder();
					var color = FormattingCodes[str[i + 1]];
					list.Add(new ColorFormattedStringComponent(str.Substring(i, fcVal), color.fg, color.bg));
					i += fcVal;
				} else if (whitespace.TryGetValue(i, out int wsVal)) {
					bool highlight = highlightAllWhitespace || (highlightLeadingAndTrailingWhitespace && (i < leadingWsIndex || i > trailingWsIndex));
					DumpBuilder();
					list.Add(new ColorFormattedStringComponent(str.Substring(i, wsVal), background: highlight ? ConsoleColor.Yellow : null));
					i += wsVal;
				} else {
					builder.Append(str[i]);
					++i;
				}

			}
			DumpBuilder();
			var output = new ColorFormattedString(list) {
				Foreground = defaultForeground,
				Background = defaultBackground
			};

			return output;

			void DumpBuilder() {
				if (builder.Length > 0) {
					var text = builder.ToString();
					list.Add(new ColorFormattedStringComponent(text));
					builder.Clear();
				}
			}
		}

		public static string Strip(string str, bool stripEscapes = true, bool stripFormatting = true, bool maintainLength = false) {
			if (!stripEscapes && !stripFormatting)
				return str;

			var escapes = stripEscapes ? StringEscaper.GetEscapeIndexes(str).ToDictionary(i => i, _ => 2) : new Dictionary<int,int>();
			var formattingCodes = stripFormatting ? FormattingCodesPattern.Matches(str).ToDictionary(m => m.Index, m => m.Length) : new Dictionary<int, int>();

			return StripInternal(str, escapes.Concat(formattingCodes), maintainLength);
		}

		private static string StripInternal(string str, IEnumerable<KeyValuePair<int, int>> dict, bool maintainLength = false) {
			if (!dict.Any())
				return str;

			var builder = new StringBuilder(str);
			int removed = 0;
			foreach (var entry in dict.OrderBy(e => e.Key)) {
				if (maintainLength) {
					for (int i = 0; i < entry.Value; ++i) {
						builder[entry.Key + i] = ' ';
					}
				} else {
					builder.Remove(entry.Key - removed, entry.Value);
					removed += entry.Value;
				}
			}
			return builder.ToString();
		}

	}
}
