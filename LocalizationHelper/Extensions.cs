﻿using System;
using System.Collections.Generic;

namespace Strut {
	static class Extensions {
		public static int CountLeading(this string input, char ch) {
			int output = 0;
			for (int i = 0; i < input.Length; ++i) {
				if (input[i] != ch)
					return output;
				++output;
			}
			return output;
		}
		public static int CountLeadingWhitespace(this string input) {
			int output = 0;
			for (int i = 0; i < input.Length; ++i) {
				if (!Char.IsWhiteSpace(input[i]))
					return output;
				++output;
			}
			return output;
		}
		public static int CountTrailing(this string input, char ch) {
			int output = 0;
			for (int i = 0; i < input.Length; ++i) {
				if (input[^(i + 1)] != ch)
					return output;
				++output;
			}
			return output;
		}
		public static int CountTrailingWhitespace(this string input) {
			int output = 0;
			for (int i = 0; i < input.Length; ++i) {
				if (!Char.IsWhiteSpace(input[^(i + 1)]))
					return output;
				++output;
			}
			return output;
		}
		public static T Clone<T>(T obj) where T : ICloneable {
			return (T)obj.Clone();
		}

		public static int FindNextWord(this string input, int currentIndex) {
			int nextSpace = input.IndexOf(' ', currentIndex);

			if (nextSpace != -1) {
				for (int i = nextSpace; i < input.Length; ++i) {
					if (input[i] != ' ')
						return i;
				}
			}
			return input.Length;
		}

		public static int FindPrevWord(this string input, int currentIndex) {
			var text = input.ToString()[..currentIndex];
			var index = text.TrimEnd().LastIndexOf(' ') + 1;

			return index;
		}

		public static IEnumerable<int> AllIndexesOf(this string str, string value) {
			if (String.IsNullOrEmpty(value))
				throw new ArgumentException("the string to find may not be empty", nameof(value));
			for (int index = 0; ; index += value.Length) {
				index = str.IndexOf(value, index);
				if (index == -1)
					break;
				yield return index;
			}
		}

		public static string NormalizeLength(this string str, int length) {
			if (str.Length == length) {
				return str;
			}
			if (str.Length < length) {
				return str.PadRight(length, ' ');
			}
			return str.Substring(0, length - 1) + '…';
		}
		public static ColorFormattedString NormalizeLength(this ColorFormattedString cfs, int length, ConsoleColor? foreground = null, ConsoleColor? background = null) {
			if (cfs.Length == length) {
				return cfs;
			}
			if (cfs.Length < length) {
				return cfs + new ColorFormattedString(new ColorFormattedStringComponent(new string(' ', length - cfs.Length), foreground, background));
			}
			return cfs.Substring(0, length - 1) + new ColorFormattedString(new ColorFormattedStringComponent("…", foreground, background));
		}
	}
}
