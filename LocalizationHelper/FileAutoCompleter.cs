﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Strut {
	class FileAutoCompleter : IAutoCompleteHandler {
		public char[] Separators { get; } = new[] { ' ' };

		public IEnumerable<string> GetSuggestions(string text, int index) {
			var word = text.Split((char[])Separators)[^1];
			if (String.IsNullOrWhiteSpace(word)) {
				return Directory.GetFileSystemEntries(".").Select(e => e[2..]).ToArray();
			} else if (Directory.Exists(word) && Path.EndsInDirectorySeparator(word)) {
				return Directory.GetFileSystemEntries(word);
			} else {
				var lIndex = word.LastIndexOfAny(new[] {Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar}) + 1;
				var path = "./" + word[..lIndex];
				if (Directory.Exists(path)) {
					var begin = Path.GetFileName(word);
					return Directory.GetFileSystemEntries(path).Where(f => Path.GetFileName(f).StartsWith(begin, StringComparison.OrdinalIgnoreCase)).Select(e => e[2..]).ToArray();
				}
			}
			return Array.Empty<string>();
		}

		public static string ReadLine(string prompt = "", string @default = "") {
			System.ReadLine.AutoCompletionHandler = new FileAutoCompleter();
			var input = System.ReadLine.Read(prompt, @default);
			System.ReadLine.AutoCompletionHandler = null;
			return input;
		}
	}
}
