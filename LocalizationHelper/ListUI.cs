﻿using SoftCircuits.Collections;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Strut {
	class ListUI<T> {
		public delegate ColorFormattedString ColorFormattedStringProvider(int index, T key, int maxStringLength, bool selected);

		private readonly OrderedDictionary<(T key, int length, bool selected), ColorFormattedString> _cfsCache = new();

		private readonly List<T> list;
		private readonly ColorFormattedStringProvider cfsProvider;


		private int currentIndex;
		private int firstDisplayIndex, lastDisplayIndex;
		private readonly int maxDisplayItems;

		private readonly ColorFormattedString heading;

		private readonly int uiTop, listTop, listLeft;

		private readonly T @default;

		public ListUI(List<T> list, ColorFormattedStringProvider cfsProvider, int maxDisplayItems = 0, int selectedIndex = 0, ColorFormattedString heading = null, T @default = default) {
			this.list = list;
			this.cfsProvider = cfsProvider;
			currentIndex = selectedIndex;
			this.maxDisplayItems = maxDisplayItems > 0 ? maxDisplayItems : Console.WindowHeight - Console.CursorTop + maxDisplayItems;
			this.heading = heading;
			this.@default = @default;

			if (heading != null)
				--this.maxDisplayItems;

			if (selectedIndex >= this.maxDisplayItems) {
				lastDisplayIndex = selectedIndex;
				firstDisplayIndex = lastDisplayIndex - this.maxDisplayItems + 1;
			} else {
				firstDisplayIndex = 0;
				lastDisplayIndex = this.maxDisplayItems - 1;
			}

			listLeft = Console.CursorLeft;
			uiTop = Console.CursorTop;
			listTop = heading != null ? uiTop + 1 : uiTop;

			Console.CursorVisible = false;
		}

		public T GetInput() {
			if (heading != null) {
				Console.SetCursorPosition(listLeft, uiTop);
				heading.Write();
			}
			RenderList();

			ConsoleKeyInfo keyInfo;
			while (true) {
				keyInfo = Console.ReadKey(true);

				switch (keyInfo.Key) {
					case ConsoleKey.Enter:
						ClearList();
						return list[currentIndex];
					case ConsoleKey.Escape:
						ClearList();
						return @default;
					case ConsoleKey.UpArrow:
						if (keyInfo.Modifiers == ConsoleModifiers.Alt) {
							ChangePage(-1);
						} else {
							ChangeIndex(-1);
						}
						break;
					case ConsoleKey.DownArrow:
						ChangeIndex(1);
						break;
					case ConsoleKey.Home:
						SetIndex(0);
						break;
					case ConsoleKey.End:
						SetIndex(list.Count - 1);
						break;
					case ConsoleKey.PageUp:
						ChangePage(-1);
						break;
					case ConsoleKey.PageDown:
						ChangePage(1);
						break;
				}
				Console.CursorVisible = false;
			}
		}

		private void RenderListIndexes(params int[] indexes) {
			int maxStringLength = Console.BufferWidth - listLeft;
			foreach (var index in indexes) {
				if (index >= list.Count || index < firstDisplayIndex || index > lastDisplayIndex)
					continue;

				int y = index - firstDisplayIndex + listTop;
				T key = list[index];
				bool isSelected = index == currentIndex;
				var cfs = GetCFS(index, key, maxStringLength, isSelected);

				if (cfs.Length > maxStringLength) {
					cfs = cfs.Substring(0, maxStringLength - 1) + new ColorFormattedString("…");
				}

				Console.SetCursorPosition(listLeft, y);
				cfs.Write();
			}
		}

		private void RenderListRange(int start, int end) {
			RenderListIndexes(Enumerable.Range(start, end - start + 1).ToArray());
		}

		private void RenderList() {
			RenderListRange(firstDisplayIndex, lastDisplayIndex);
		}

		private void ClearList() {
			Console.SetCursorPosition(listLeft, uiTop);
			Console.Write(new string(' ', Console.WindowWidth * maxDisplayItems));
			Console.SetCursorPosition(listLeft, uiTop);
		}

		private void SetIndex(int index, bool topAlign = false, bool bottomAlign = false) {
			index = Math.Clamp(index, 0, list.Count - 1);
			if (index == currentIndex)
				return;

			int prevIndex = currentIndex;
			currentIndex = index;
			if (topAlign || index > lastDisplayIndex) {
				lastDisplayIndex = Math.Min(list.Count - 1, currentIndex + maxDisplayItems - 1);
				firstDisplayIndex = lastDisplayIndex - maxDisplayItems + 1;
				RenderList();
			} else if (bottomAlign || index < firstDisplayIndex) {
				firstDisplayIndex = Math.Max(0, currentIndex - maxDisplayItems + 1);
				lastDisplayIndex = firstDisplayIndex + maxDisplayItems - 1;
				RenderList();
			} else {
				RenderListIndexes(prevIndex, index);
			}

		}

		private void ChangeIndex(int amount, bool topAlign = false, bool bottomAlign = false) {
			SetIndex(currentIndex + amount, topAlign, bottomAlign);
		}

		private void ChangePage(int amount) {
			bool top = true;
			if (amount == 0)
				return;
			int index = (currentIndex / maxDisplayItems + amount) * maxDisplayItems;
			if (amount < 0) {
				index += maxDisplayItems - 1;
				top = false;
			}
			SetIndex(index, top, !top);
		}

		private ColorFormattedString GetCFS(int index, T key, int maxStringLength, bool selected) {
			var tuple = (key, maxStringLength, selected);
			if (_cfsCache.TryGetValue(tuple, out var cfs)) {
				return cfs;
			}
			cfs = cfsProvider.Invoke(index, key, maxStringLength, selected);
			_cfsCache[tuple] = cfs;
			while (_cfsCache.Count > list.Count * 2) {
				_cfsCache.RemoveAt(0);
			}
			return cfs;
		}
	}
}
