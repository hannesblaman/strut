﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Strut {
	class PlaceholderHandler : IAutoCompleteHandler {

		public static Regex Pattern { get; } = new Regex(@"(%[\w\-]+%)|(%[\d.,(+\-#%$ ]+(?:[A-Ha-hNnOoSsXx]|[Tt][A-DF-IMNQRSTYZa-ehjklmprsyz]))|(%[\w\-]+)|(\$\{[\w:.%<>^=+\-, ]*\})|(\{[\w:.%<>^=+\-, ]*\})|(\[[\w:.%<>^=+\-, ]*\])");

		public PlaceholderHandler(string original) {
			Placeholders = Pattern.Matches(original).Select(m => m.Value).ToHashSet();
		}

		public HashSet<string> Placeholders { get; }

		public char[] Separators { get; } = new[] { ' ' };

		public IEnumerable<string> GetSuggestions(string text, int index) {
			var word = text.Split(Separators)[^1];
			var begunPlaceholders = new HashSet<string>();
			var otherPlaceholders = new HashSet<string>();
			foreach (var p in Placeholders) {
				if (word.EndsWith(p, StringComparison.OrdinalIgnoreCase)) {
					return new[] { word[..^p.Length] + p };
				}
				bool found = false;
				for (int i = 1; i < p.Length; ++i) {
					if (word.EndsWith(p[..i], StringComparison.OrdinalIgnoreCase)) {
						begunPlaceholders.Add(word[..^i] + p);
						found = true;
						break;
					}
				}
				if (!found) {
					otherPlaceholders.Add(word + p);
				}
			}
			return begunPlaceholders.Concat(otherPlaceholders).ToArray();
		}
	}
}
