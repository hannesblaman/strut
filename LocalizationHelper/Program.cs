﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SharpYaml.Serialization;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using static Strut.Extensions;

namespace Strut {
	class Program {
		static readonly Serializer serializer = new(new SerializerSettings{
			EmitTags = false
		});
		static readonly ColorFormatter inputColorFormatter = new();
		static readonly ColorFormatter inconsistencyColorFormatter = new() { Foreground = ConsoleColor.Red, HighlightLeadingAndTrailingWhitespace = false };

		static string sourceFile;

		static List<object[]> keys;
		static Dictionary<object[], string> sourceDict;
		static Dictionary<object[], string> targetDict;

		static DateTime time = DateTime.Now;
		static void Main(string[] args) {
			Console.CursorVisible = false;
			Console.OutputEncoding = Encoding.UTF8;

			sourceFile = FileAutoCompleter.ReadLine("Source file: ") ?? "";

			ClearWindow();


			var contents = File.ReadAllText(sourceFile);
			var rawDict = serializer.Deserialize<Dictionary<object, object>>(contents);
			sourceDict = FlattenDictionary(rawDict);
			keys = sourceDict.Keys.ToList();


			var autosavedFile = CheckAutoSaved(sourceFile);
			if (autosavedFile != null) {
				var asContents = File.ReadAllText(autosavedFile);
				var asRawDict = JObject.Parse(asContents);
				targetDict = new Dictionary<object[], string>(FlattenDictionary(asRawDict), new ArrayComparer<object>());
			} else {
				targetDict = new Dictionary<object[], string>(new ArrayComparer<object>());
			}


			ClearWindow();

			TranslationLoop(false);

		Save:
			ClearWindow();

			var missingTranslations = keys.Except(targetDict.Keys, new ArrayComparer<object>()).ToList();
			if (missingTranslations.Count > 0) {
				var fg = Console.ForegroundColor;
				Console.ForegroundColor = ConsoleColor.Red;
				Console.WriteLine($"{missingTranslations.Count} strings are missing a translation. Press Enter to review your translations, or Escape to ignore.");
				Console.ForegroundColor = fg;

				ConsoleKeyInfo keyInfo;
				while (true) {
					keyInfo = Console.ReadKey(true);
					if (keyInfo.Key == ConsoleKey.Enter) {
						ClearWindow();
						TranslationLoop(true);
						goto Save;
					} else if (keyInfo.Key == ConsoleKey.Escape) {
						break;
					}
				}
			}

			ClearWindow();

			ReadLine.ClearHistory();
			ReadLine.AddHistory(Path.Combine("output", EscapePath(sourceFile, false)).Replace(Path.AltDirectorySeparatorChar, Path.DirectorySeparatorChar));
			ReadLine.ColorFormattingHandler = null;
			var outputFile = FileAutoCompleter.ReadLine("All done! Output file: ");
			Directory.CreateDirectory(Path.GetDirectoryName(outputFile));
			Save(outputFile, targetDict);
		}

		static void TranslationLoop(bool openStringList = false) {
			int i = Math.Max(keys.FindIndex(index => !targetDict.ContainsKey(index)), 0);

			if (openStringList) {
				var newIndex = SelectString(i);
				if (newIndex != -1) {
					i = newIndex;
				}
			}

			while (true) {
				if (i >= keys.Count)
					break;
				if (GetString(i) == null) {
					ClearWindow();
					var newIndex = SelectString(i);
					if (newIndex != -1) {
						i = newIndex;
					}
				} else {
					AutoSave(sourceFile, time, targetDict);
					++i;
				}
			}
		}

		static string GetString(int index) {
			var key = keys[index];
			var source = sourceDict[key];
			var target = targetDict.GetValueOrDefault(key);

			ClearWindow();
			Console.ForegroundColor = ConsoleColor.Gray;
			Console.Write($"{index + 1}/{keys.Count} • ");
			WriteKey(key);
			Console.WriteLine();

			Console.BackgroundColor = ConsoleColor.White;
			Console.ForegroundColor = ConsoleColor.Black;
			WriteString(source);
			Console.BackgroundColor = ConsoleColor.Black;
			Console.ForegroundColor = ConsoleColor.White;


			Console.WriteLine();
			Console.WriteLine();
			Console.Write("> ");

			var inputStartPosition = Console.GetCursorPosition();
			string inputString = target != null ? StringEscaper.EscapeString(target) : "";
		Rewrite:

			Console.ForegroundColor = inputColorFormatter.Foreground;
			Console.BackgroundColor = inputColorFormatter.Background;

			ReadLine.ClearHistory();
			ReadLine.AddHistory(StringEscaper.EscapeString(source));
			ReadLine.AutoCompletionHandler = new PlaceholderHandler(source);
			ReadLine.ColorFormattingHandler = inputColorFormatter;
			string input = ReadLine.Read(@default: inputString);
			if (input == null) {
				return null;
			}
			if (input.Length != 0 || source.Length == 0) {
				var unescaped = StringEscaper.UnescapeString(input);

				if (input != inputString) {
					var consistency = TranslationConsistency.IsTranslationConsistent(source, unescaped);

					if (!consistency.Consistent) {
						ReadLine.ColorFormattingHandler = inconsistencyColorFormatter;
						Console.CursorVisible = false;
						Console.SetCursorPosition(0, 10);
						Console.WriteLine(new string(' ', (Console.WindowHeight - Console.CursorTop - 1) * Console.WindowWidth));
						Console.SetCursorPosition(0, 10);
						var dsa = "Inconsistencies (press Enter again to ignore):\n" + String.Join('\n', consistency.Inconsistencies.Select(s => " • " + s.Replace("\n", "\n   ")));

						WriteString(inconsistencyColorFormatter.GetColorFormattedString(dsa));
						Console.SetCursorPosition(inputStartPosition.Left, inputStartPosition.Top);
						Console.CursorVisible = true;

						inputString = input;
						goto Rewrite;
					}
				}
				return targetDict[key] = unescaped;
			}
			return "";
		}

		static int SelectString(int currentIndex) {
			int indexLength = keys.Count.ToString().Length;
			int length = Console.WindowWidth - 8;
			int keyLength = Math.Min(length / 4, 40);
			int srcLength = (length - keyLength) / 2;
			int trgLength = length - keyLength - srcLength;

			var stringsUi = new ListUI<int>(Enumerable.Range(0, keys.Count).ToList(), (index, i_key, maxStringLength, isSelected) => {
				var key = keys[i_key];
				string src = sourceDict[key];
				string trg = targetDict.GetValueOrDefault(key);

				var keyCfs = (new ColorFormattedString((index + 1).ToString().PadLeft(indexLength) + ". ") + KeyToCFS(key, isSelected ? ConsoleColor.Black : ConsoleColor.Gray, ConsoleColor.DarkGray)).NormalizeLength(keyLength, foreground: ConsoleColor.DarkGray);
				var srcCfs = ColorFormatter.ColorFormatString(StringEscaper.EscapeString(src)).NormalizeLength(srcLength);
				var trgCfs = (trg != null
					? ColorFormatter.ColorFormatString(StringEscaper.EscapeString(trg))
					: new ColorFormattedString(new[] { new ColorFormattedStringComponent(" - none - ", ConsoleColor.DarkGray) })).NormalizeLength(trgLength);

				var cfs = ColorFormattedString.Empty + " " + keyCfs + "   " + srcCfs + "   " + trgCfs + " ";

				cfs.Background = isSelected ? ConsoleColor.White : ConsoleColor.Black;
				cfs.Foreground = isSelected ? ConsoleColor.Black : ConsoleColor.White;

				return cfs;
			}, selectedIndex: currentIndex,
			heading: new ColorFormattedString(" " + new string(' ', indexLength + 2) + "Key".NormalizeLength(keyLength - indexLength - 2) + "   " + "Source".NormalizeLength(srcLength) + "   " + "Translation".NormalizeLength(trgLength) + " ") {
				Foreground = ConsoleColor.White
			},
			@default: -1);

			return stringsUi.GetInput();
		}

		static void Save(string location, Dictionary<object[], string> list) {
			if (list.Count > 0) {
				File.WriteAllText(location, serializer.Serialize(UnflattenDictionary(list)));
			}
		}
		static void AutoSave(string sourceFileLocation, DateTime time, Dictionary<object[], string> list) {
			if (list.Count > 0) {
				var path = Path.Combine("autosave", EscapePath(sourceFileLocation) + $"-{time:yyyyMMddHHmmss}.json");
				Directory.CreateDirectory(Path.GetDirectoryName(path));
				File.WriteAllText(path, JsonConvert.SerializeObject(UnflattenDictionary(list)));
			}
		}

		static void ClearWindow() {
			Console.SetCursorPosition(0, 0);
			Console.Write(new string(' ', Console.WindowWidth * Console.WindowHeight));
			Console.SetCursorPosition(0, 0);
		}

		static string EscapePath(string path, bool escapeExtension = true) {
			string extension = "";
			if (!escapeExtension) {
				extension = Path.GetExtension(path);
				path = path[..^extension.Length];
			}

			var builder = new StringBuilder();
			var invalidChars = @"<>:""/\|?*.";
			foreach (char c in path) {
				if (invalidChars.Contains(c)) {
					builder.Append('_');
				} else {
					builder.Append(c);
				}
			}
			return builder.Append(extension).ToString();
		}

		static string CheckAutoSaved(string sourceFileLocation) {
			sourceFileLocation = sourceFileLocation.Replace(Path.AltDirectorySeparatorChar, Path.DirectorySeparatorChar);
			var path = Path.Combine("autosave", EscapePath(sourceFileLocation));
			var dir = Path.GetDirectoryName(path);
			if (!Directory.Exists(dir)) {
				return null;
			}
			var autosaves = Directory.GetFiles(dir).Where(f => f.StartsWith(path)).Reverse().ToArray();
			if (autosaves.Length == 0) {
				return null;
			}

			var autosaveNames = new List<string> {};

			for (int i = 0; i < autosaves.Length; ++i) {
				try {
					var date = Path.GetFileNameWithoutExtension(autosaves[i]).Split('-')[^1];
					var datetime = DateTime.ParseExact(date, "yyyyMMddHHmmss", CultureInfo.InvariantCulture);
					autosaveNames.Add($" {(i + 1).ToString().PadLeft(autosaves.Length.ToString().Length)}. {datetime} ");
				} catch { }
			}

			int maxLength = autosaveNames.Max(n => n.Length);

			var listUi = new ListUI<int>(Enumerable.Range(0, autosaveNames.Count + 1).ToList(), (_, key, _, isSelected) => {
				if (key == 0) {
					return new ColorFormattedString(" - Create new - ") {
						Background = isSelected ? ConsoleColor.White : ConsoleColor.Black,
						Foreground = isSelected ? ConsoleColor.Black : ConsoleColor.DarkGray,
					}.NormalizeLength(maxLength);
				}
				return new ColorFormattedString(autosaveNames[key - 1]) {
					Background = isSelected ? ConsoleColor.White : ConsoleColor.Black,
					Foreground = isSelected ? ConsoleColor.Black : ConsoleColor.Gray
				}.NormalizeLength(maxLength);
			}, heading: new ColorFormattedString($"{autosaveNames.Count} autosave(s) for {sourceFileLocation} were found") {
				Foreground = ConsoleColor.White
			});

			var input = listUi.GetInput();

			if (input == 0)
				return null;
			try {
				return autosaves[input - 1];
			} catch {
				return null;
			}
		}

		static Dictionary<object, object> UnflattenDictionary(Dictionary<object[], string> input) {
			var startDepth = input.Keys.Max(k => k.Length);

			var _input = input.Select(p => KeyValuePair.Create(Clone(p.Key), (object)Clone(p.Value))).ToDictionary(p => p.Key, p => p.Value);
			var dict = UnflattenDictionaryInternal(_input, startDepth);
			return dict.Select(p => KeyValuePair.Create(p.Key[0], p.Value))
				.ToDictionary(p => p.Key, p => p.Value);
		}

		internal static IEnumerable<KeyValuePair<object[], object>> UnflattenDictionaryInternal(IEnumerable<KeyValuePair<object[], object>> input, int depth) {
			var output = new Dictionary<object[], object>(new ArrayComparer<object>());
			if (depth == 1) {
				return input;
			}
			foreach (var pair in input) {
				if (pair.Key.Length < depth) {
					output[pair.Key] = pair.Value;
					continue;
				}
				var parent = pair.Key.Take(depth - 1).ToArray();
				var child = pair.Key.Last();
				var coll = output.GetValueOrDefault(parent);
				if (coll == null) {
					coll = new SortedDictionary<object, object>();
					output[parent] = coll;
				}
				((SortedDictionary<object, object>)coll).Add(child, pair.Value);
			}
			return UnflattenDictionaryInternal(output.Select(p => {
				if (p.Value is SortedDictionary<object, object> dict && dict.Keys.First() is int) {
					return KeyValuePair.Create(p.Key, (object)new List<object>(dict.Values));
				}
				return p;
			}), depth - 1);
		}

		static Dictionary<object[], string> FlattenDictionary(Dictionary<object, object> input) {
			var output = new Dictionary<object[], string>(new ArrayComparer<object>());
			foreach (var pair in FlattenDictionaryInternal(input, Array.Empty<object>())) {
				output[pair.Key.ToArray()] = pair.Value;
			}
			return output;
		}

		internal static Dictionary<IEnumerable<object>, string> FlattenDictionaryInternal(IDictionary<object, object> input, IEnumerable<object> prefix) {
			var output = new Dictionary<IEnumerable<object>, string>();
			foreach (var pair in input) {
				var fullKey = prefix.Concat(new[] {pair.Key });
				if (pair.Value is IList<object> list) {
					for (int i = 0; i < list.Count; ++i) {
						output[fullKey.Concat(new object[] { i })] = (string)list[i];
					}
				} else if (pair.Value is IDictionary<object, object> dict) {
					foreach (var nPair in FlattenDictionaryInternal(dict, fullKey)) {
						output[nPair.Key] = nPair.Value;
					}
				} else {
					output[fullKey] = pair.Value.ToString();
				}
			}
			return output;
		}

		static Dictionary<object[], string> FlattenDictionary(JObject input) {
			var output = new Dictionary<object[], string>(new ArrayComparer<object>());
			foreach (var pair in FlattenDictionaryInternal(input, Array.Empty<object>())) {
				output[pair.Key.ToArray()] = pair.Value;
			}
			return output;
		}

		internal static Dictionary<IEnumerable<object>, string> FlattenDictionaryInternal(JObject input, IEnumerable<object> prefix) {
			var output = new Dictionary<IEnumerable<object>, string>();
			foreach (var pair in input) {
				var fullKey = prefix.Concat(new[] {pair.Key });
				if (pair.Value is JArray list) {
					for (int i = 0; i < list.Count; ++i) {
						output[fullKey.Concat(new object[] { i })] = (string)list[i];
					}
				} else if (pair.Value is JObject dict) {
					foreach (var nPair in FlattenDictionaryInternal(dict, fullKey)) {
						output[nPair.Key] = nPair.Value;
					}
				} else {
					output[fullKey] = pair.Value.ToString();
				}
			}
			return output;
		}

		static void WriteKey(object[] key, ConsoleColor primary = ConsoleColor.Gray, ConsoleColor secondary = ConsoleColor.DarkGray) {
			var cfs = KeyToCFS(key, primary, secondary);
			cfs.Write();
		}

		static string KeyToString(object[] key) {
			var builder = new StringBuilder();
			for (var i = 0; i < key.Length; ++i) {
				var obj = key[i];
				if (obj is int) {
					builder.Append('[');
					builder.Append(obj);
					builder.Append(']');
				} else {
					if (i > 0) {
						builder.Append('→');
					}
					builder.Append(obj);
				}
			}
			return builder.ToString();
		}

		static ColorFormattedString KeyToCFS(object[] key, ConsoleColor primary = ConsoleColor.Gray, ConsoleColor secondary = ConsoleColor.DarkGray) {
			var components = new List<ColorFormattedStringComponent>();
			for (var i = 0; i < key.Length; ++i) {
				var obj = key[i];
				if (obj is int) {
					components.Add(new ColorFormattedStringComponent("[", secondary));
					components.Add(new ColorFormattedStringComponent(obj.ToString(), primary));
					components.Add(new ColorFormattedStringComponent("]", secondary));
				} else {
					if (i > 0) {
						components.Add(new ColorFormattedStringComponent("→", secondary));
					}
					components.Add(new ColorFormattedStringComponent(obj.ToString(), primary));
				}
			}
			return new ColorFormattedString(components);
		}

		static void WriteString(string str) {
			var escapedStr = StringEscaper.EscapeString(str, true);
			var formattedString = ColorFormatter.ColorFormatString(escapedStr, ConsoleColor.Black, ConsoleColor.White);
			WriteString(formattedString);
		}
		static void WriteString(ColorFormattedString str) {
			foreach (var component in str) {
				Console.ForegroundColor = component.Foreground ?? str.Foreground;
				Console.BackgroundColor = component.Background ?? str.Background;
				Console.Write(component.String);
			}
		}
	}
}
