﻿using System.Collections.Generic;
using System.Text;

namespace Strut {
	static class StringEscaper {
		public static Dictionary<char, char> Escapes { get; } = new() {
			{ '\\', '\\' },
			{ '\r', 'r' },
			{ '\n', 'n' },
			{ '\t', 't' },
			{ '\b', 'b' },
			{ '\f', 'f' },
			{ '\v', 'v' },
			{ '\0', '0' }
		};
		public static string EscapeString(string input, bool keepEscapedChars = false) {
			var builder = new StringBuilder();
			for (int i = 0; i < input.Length; ++i) {
				char c = input[i];
				bool any = false;
				foreach (var pair in Escapes) {
					if (c == pair.Key) {
						builder.Append('\\').Append(pair.Value);
						if (keepEscapedChars)
							builder.Append(pair.Key);
						any = true;
						break;
					}
				}
				if (!any) {
					builder.Append(c);
				}
			}
			return builder.ToString();
		}
		public static string UnescapeString(string input) {
			var builder = new StringBuilder();
			bool esc = false;
			for (int i = 0; i < input.Length; ++i) {
				char c = input[i];
				if (esc) {
					bool any = false;
					foreach (var pair in Escapes) {
						if (c == pair.Value) {
							builder.Append(pair.Key);
							any = true;
							break;
						}
					}
					if (!any) {
						builder.Append('\\');
					}
					esc = false;
				} else {
					if (c == '\\') {
						esc = true;
						continue;
					} else {
						builder.Append(c);
					}
				}
			}
			return builder.ToString();
		}
		public static IEnumerable<int> GetEscapeIndexes(string input) {
			bool esc = false;
			for (int i = 0; i < input.Length; ++i) {
				char c = input[i];
				if (esc) {
					foreach (var pair in Escapes) {
						if (c == pair.Value) {
							yield return i - 1;
							break;
						}
					}
					esc = false;
				} else if (c == '\\') {
					esc = true;
					continue;
				}
			}
		}

	}
}
