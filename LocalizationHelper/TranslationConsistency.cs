﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Strut {
	class TranslationConsistency {
		class StringProperties {
			public StringProperties(string str) {
				String = str ?? throw new ArgumentNullException(nameof(str));
				StrippedString = ColorFormatter.Strip(str, false, true, false);
				Length = String.Length;

				var leadingWsCount = String.CountLeadingWhitespace();
				LeadingWhitespace = String[..leadingWsCount];
				var trailingWsCount = String.CountTrailingWhitespace();
				TrailingWhitespace = String[^trailingWsCount..];

				string strippedStringMaintainedLength = ColorFormatter.Strip(str, false, true, true);
				var strippedLeadingWsCount = strippedStringMaintainedLength.CountLeadingWhitespace();
				StrippedLeadingWhitespace = String[..strippedLeadingWsCount];
				var strippedTrailingWsCount = strippedStringMaintainedLength.CountTrailingWhitespace();
				StrippedTrailingWhitespace = String[^strippedTrailingWsCount..];

				StrippedLeadingWhitespaceDisplayLength = StrippedString.CountLeadingWhitespace();
				StrippedTrailingWhitespaceDisplayLength = StrippedString.CountTrailingWhitespace();

				int newlines = 0;
				int letters = 0;
				int upper = 0;
				foreach (var c in String) {
					if (c == '\n') {
						++newlines;
					} else if (Char.IsLetter(c)) {
						++letters;
						if (Char.IsUpper(c)) {
							++upper;
						}
					}
				}
				NewlineCount = newlines;
				UppercaseAmount = upper;
				UppercasePercent = letters == 0 ? 0 : upper * 100 / letters;

				var placeholders = PlaceholderHandler.Pattern.Matches(String);
				PlaceholderCounts = placeholders.GroupBy(p => p.Value).ToImmutableDictionary(g => g.Key, g => g.Count());

			}

			public string String { get; }
			public string StrippedString { get; }
			public int Length { get; }
			public string LeadingWhitespace { get; }
			public string TrailingWhitespace { get; }
			public string StrippedLeadingWhitespace { get; }
			public string StrippedTrailingWhitespace { get; }
			public int StrippedLeadingWhitespaceDisplayLength { get; }
			public int StrippedTrailingWhitespaceDisplayLength { get; }
			public int NewlineCount { get; }
			public int UppercaseAmount { get; }
			public int UppercasePercent { get; }
			public ImmutableDictionary<string, int> PlaceholderCounts { get; }
		}

		public static float LengthToleranceFactor { get; set; } = 1.5f;
		public static int LengthToleranceTerm { get; set; } = 3;
		public static float UppercasePercentToleranceFactor { get; set; } = 1.5f;
		public static int UppercaseAmountToleranceTerm { get; set; } = 3;

		public static (bool Consistent, string[] Inconsistencies) IsTranslationConsistent(string source, string translation) {
			if (source == translation)
				return (true, Array.Empty<string>());

			var srcProp = new StringProperties(source);
			var trsProp = new StringProperties(translation);
			var inconsistencies = new List<string>();

			if (Math.Abs(srcProp.Length - trsProp.Length) > LengthToleranceTerm) {
				if (trsProp.Length > srcProp.Length * LengthToleranceFactor) {
					inconsistencies.Add("Translation is significantly longer than source.");
				}
				if (srcProp.Length > trsProp.Length * LengthToleranceFactor) {
					inconsistencies.Add("Source is significantly longer than translation.");
				}
			}

			if (Math.Abs(srcProp.UppercaseAmount - trsProp.UppercaseAmount) > UppercaseAmountToleranceTerm) {
				if (trsProp.UppercasePercent > srcProp.UppercasePercent * UppercasePercentToleranceFactor) {
					inconsistencies.Add($"Translation has significantly more uppercase letters than source.");
				}
				if (srcProp.UppercasePercent > trsProp.UppercasePercent * UppercasePercentToleranceFactor) {
					inconsistencies.Add($"Source has significantly more uppercase letters than translation.");
				}
			}

			if (srcProp.StrippedLeadingWhitespace != trsProp.StrippedLeadingWhitespace && srcProp.StrippedLeadingWhitespaceDisplayLength != 0 && trsProp.StrippedLeadingWhitespaceDisplayLength != 0) {
				inconsistencies.Add(@$"Leading whitespace is inconsistent:
       Source: ""{StringEscaper.EscapeString(srcProp.StrippedLeadingWhitespace)}"" (length: {srcProp.StrippedLeadingWhitespace.Length}, displayed length: {srcProp.StrippedLeadingWhitespaceDisplayLength})
  Translation: ""{StringEscaper.EscapeString(trsProp.StrippedLeadingWhitespace)}"" (length: {trsProp.StrippedLeadingWhitespace.Length}, displayed length: {trsProp.StrippedLeadingWhitespaceDisplayLength})");
			}
			if (srcProp.StrippedTrailingWhitespace != trsProp.StrippedTrailingWhitespace && srcProp.StrippedTrailingWhitespaceDisplayLength != 0 && trsProp.StrippedTrailingWhitespaceDisplayLength != 0) {
				inconsistencies.Add(@$"Trailing whitespace is inconsistent:
       Source: ""{StringEscaper.EscapeString(srcProp.StrippedTrailingWhitespace)}"" (length: {srcProp.StrippedTrailingWhitespace.Length}, displayed length: {srcProp.StrippedTrailingWhitespaceDisplayLength})
  Translation: ""{StringEscaper.EscapeString(trsProp.StrippedTrailingWhitespace)}"" (length: {trsProp.StrippedTrailingWhitespace.Length}, displayed length: {trsProp.StrippedTrailingWhitespaceDisplayLength})");
			}
			if (srcProp.NewlineCount != trsProp.NewlineCount) {
				inconsistencies.Add($"Source has {srcProp.NewlineCount} newline(s) (\\n), whereas translation has {trsProp.NewlineCount} newline(s).");
			}

			var placeholderInconsistencies = new List<string>();
			var keys = srcProp.PlaceholderCounts.Keys.Concat(trsProp.PlaceholderCounts.Keys).ToHashSet();
			foreach (var key in keys) {
				if (srcProp.PlaceholderCounts.GetValueOrDefault(key) != trsProp.PlaceholderCounts.GetValueOrDefault(key)) {
					placeholderInconsistencies.Add(key);
				}
			}
			if (placeholderInconsistencies.Count > 0) {
				var builder = new StringBuilder();
				builder.AppendLine("Placeholder usage is inconsistent:");
				foreach (var key in placeholderInconsistencies) {
					builder.AppendLine($"  {key} is used {srcProp.PlaceholderCounts.GetValueOrDefault(key)} time(s) in source " +
						$"but {trsProp.PlaceholderCounts.GetValueOrDefault(key)} time(s) in translation.");
				}
				inconsistencies.Add(builder.ToString());
			}

			return (inconsistencies.Count == 0, inconsistencies.ToArray());
		}
	}
}
