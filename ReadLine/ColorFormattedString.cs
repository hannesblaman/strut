﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;

namespace System {
	public class ColorFormattedString : IEnumerable<ColorFormattedStringComponent>, IEquatable<ColorFormattedString> {
		private IEnumerable<ColorFormattedStringComponent> _list;

		public ColorFormattedString() {
			_list = new List<ColorFormattedStringComponent>();
		}
		public ColorFormattedString(string str) {
			if (String.IsNullOrEmpty(str)) {
				_list = new List<ColorFormattedStringComponent>();
			} else {
				_list = new List<ColorFormattedStringComponent> { new ColorFormattedStringComponent(str) };
			}
			//SplitNewlines();
		}
		public ColorFormattedString(ColorFormattedStringComponent component) {
			_list = new List<ColorFormattedStringComponent> { component };
		}
		public ColorFormattedString(IEnumerable<ColorFormattedStringComponent> list) {
			_list = list;
			Simplify();
			//SplitNewlines();
		}

		public ConsoleColor Foreground { get; set; } = DefaultForeground;
		public ConsoleColor Background { get; set; } = DefaultBackground;

		public const ConsoleColor DefaultForeground = ConsoleColor.White;
		public const ConsoleColor DefaultBackground = ConsoleColor.Black;

		public static readonly ColorFormattedString Empty = new ColorFormattedString();

		public int Length {
			get {
				int length = 0;
				foreach (var component in _list) {
					length += component.String.Length;
				}
				return length;
			}
		}

		public void Write() {
			Write(Console.Out);
		}

		public void Write(TextWriter writer) {
			var fg = Console.ForegroundColor;
			var bg = Console.BackgroundColor;
			foreach (var component in _list) {
				Console.ForegroundColor = component.Foreground ?? Foreground;
				Console.BackgroundColor = component.Background ?? Background;
				Console.Write(component.String);
			}
			Console.ForegroundColor = fg;
			Console.BackgroundColor = bg;
		}

		private void SplitNewlines() {
			var list = new List<ColorFormattedStringComponent>();
			foreach (var component in _list) {
				var split = component.String.Split('\n');
				if (split.Length == 1) {
					list.Add(component);
					continue;
				}
				list.Add(new ColorFormattedStringComponent(split[0], component.Foreground, component.Background));
				foreach (var part in split) {
					list.Add(new ColorFormattedStringComponent(" \n", background: DefaultBackground));
					if (part.Length > 0)
						list.Add(new ColorFormattedStringComponent(part, component.Foreground, component.Background));
				}
			}
			list.Add(new ColorFormattedStringComponent(" ", background: DefaultBackground));
			_list = list;
		}

		private void Simplify() {
			var list = _list.ToList();
			for (int i = 1; i < list.Count;) {
				var prev = list[i-1];
				var curr = list[i];
				if (prev.Background == curr.Background && prev.Foreground == curr.Foreground) {
					list[i - 1] = new ColorFormattedStringComponent(prev.String + curr.String, prev.Foreground, prev.Background);
					list.RemoveAt(i);
				} else {
					++i;
				}
			}
			_list = list.Where(component => component.Length != 0).ToList();
		}

		public IEnumerator<ColorFormattedStringComponent> GetEnumerator() => _list.GetEnumerator();
		public override string ToString() {
			var builder = new StringBuilder();
			foreach (var component in _list) {
				builder.Append(component.String);
			}
			return builder.ToString();
		}
		IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

		public ColorFormattedString Substring(int startIndex) {
			return new ColorFormattedString(InternalSubstring(startIndex, Length - startIndex));
		}
		public ColorFormattedString Substring(int startIndex, int length) {
			return new ColorFormattedString(InternalSubstring(startIndex, length));
		}

		private IEnumerable<ColorFormattedStringComponent> InternalSubstring(int startIndex, int length) {
			int _len = Length;

			if (startIndex < 0) {
				throw new ArgumentOutOfRangeException(nameof(startIndex));
			}

			if (startIndex > _len) {
				throw new ArgumentOutOfRangeException(nameof(startIndex));
			}

			if (length < 0) {
				throw new ArgumentOutOfRangeException(nameof(length));
			}

			if (startIndex > _len - length) {
				throw new ArgumentOutOfRangeException(nameof(length));
			}

			if (_len == 0) {
				yield break;
			}

			if (startIndex == 0 && length == _len) {
				foreach (var component in _list) {
					yield return component;
				}
				yield break;
			}

			int endIndex = startIndex + length;

			int i = 0;
			foreach (var component in _list) {
				if (i + component.Length >= startIndex) {
					int start = Math.Max(0, startIndex-i);
					int end = Math.Clamp(endIndex - i - start, 0, component.Length - start);
					yield return new ColorFormattedStringComponent(component.String.Substring(start, end), component.Foreground, component.Background);
				}
				i += component.Length;
			}
		}

		public override bool Equals(object obj) => Equals(obj as ColorFormattedString);
		public bool Equals(ColorFormattedString other) => other != null && EqualityComparer<IEnumerable<ColorFormattedStringComponent>>.Default.Equals(_list, other._list) && Foreground == other.Foreground && Background == other.Background;
		public override int GetHashCode() => HashCode.Combine(_list, Foreground, Background);

		public static bool operator ==(ColorFormattedString left, ColorFormattedString right) => EqualityComparer<ColorFormattedString>.Default.Equals(left, right);
		public static bool operator !=(ColorFormattedString left, ColorFormattedString right) => !(left == right);

		public static ColorFormattedString operator +(ColorFormattedString left, ColorFormattedString right) => new ColorFormattedString(left._list.Concat(right._list)) { Background = left.Background, Foreground = left.Foreground };
		public static ColorFormattedString operator +(ColorFormattedString left, ColorFormattedStringComponent right) => new ColorFormattedString(left._list.Append(right)) { Background = left.Background, Foreground = left.Foreground };
		public static ColorFormattedString operator +(ColorFormattedString left, string right) => new ColorFormattedString(left._list.Append(new ColorFormattedStringComponent(right))) { Background = left.Background, Foreground = left.Foreground };
	}
}
