﻿using System.Collections.Generic;

namespace System {
	public class ColorFormattedStringComponent : IEquatable<ColorFormattedStringComponent> {
		public ColorFormattedStringComponent(string @string, ConsoleColor? foreground = null, ConsoleColor? background = null) {
			String = @string ?? "";
			Foreground = foreground;
			Background = background;
		}

		public ConsoleColor? Foreground { get; }
		public ConsoleColor? Background { get; }
		public string String { get; }

		public int Length { get => String.Length; }

		public override bool Equals(object obj) => Equals(obj as ColorFormattedStringComponent);
		public bool Equals(ColorFormattedStringComponent other) => other != null && Foreground == other.Foreground && Background == other.Background && String == other.String;
		public override int GetHashCode() => HashCode.Combine(Foreground, Background, String);
		public override string ToString() => String;

		public static bool operator ==(ColorFormattedStringComponent left, ColorFormattedStringComponent right) => EqualityComparer<ColorFormattedStringComponent>.Default.Equals(left, right);
		public static bool operator !=(ColorFormattedStringComponent left, ColorFormattedStringComponent right) => !(left == right);
	}
}
