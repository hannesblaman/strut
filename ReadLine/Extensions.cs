﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Strut {
	static class Extensions {
		public static TValue GetOrDefault<TKey, TValue>(this Dictionary<TKey, TValue> dict, TKey key) {
			if (dict.ContainsKey(key))
				return dict[key];
			return default;
		}

		public static int CountLeading(this string input, char ch) {
			var output = 0;
			for (int i = 0; i < input.Length; ++i) {
				if (input[i] != ch)
					return output;
				++output;
			}
			return output;
		}
		public static int CountTrailing(this string input, char ch) {
			var output = 0;
			for (int i = 0; i < input.Length; ++i) {
				if (input[^(i + 1)] != ch)
					return output;
				++output;
			}
			return output;
		}
		public static T Clone<T>(T obj) where T : ICloneable {
			return (T)obj.Clone();
		}

		public static int FindNextWord(this string input, int currentIndex) {
			int nextSpace = input.IndexOf(' ', currentIndex);

			if (nextSpace != -1) {
				for (int i = nextSpace; i < input.Length; ++i) {
					if (input[i] != ' ')
						return i;
				}
			}
			return input.Length;
		}

		public static int FindPrevWord(this string input, int currentIndex) {
			var text = input.ToString()[..currentIndex];
			var index = text.TrimEnd().LastIndexOf(' ') + 1;

			return index;
		}
	}
}
