using System.Collections.Generic;

namespace System
{
    public interface IAutoCompleteHandler
    {
        char[] Separators { get; }
        IEnumerable<string> GetSuggestions(string text, int index);
    }
}