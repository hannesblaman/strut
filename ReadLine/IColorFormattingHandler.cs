﻿namespace System {
	public interface IColorFormattingHandler {
		ColorFormattedString GetColorFormattedString(string str);
	}
}
