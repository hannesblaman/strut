using Strut;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Internal.ReadLine {
	internal class KeyHandler {
		private IColorFormattingHandler _colorFormattingHandler;

		private int _cursorPos;
		private int _cursorLimit;
		private StringBuilder _text;
		private ColorFormattedString _cfs = null;
		private List<string> _history;
		private int _historyIndex;
		private ConsoleKeyInfo _keyInfo;
		private Dictionary<string, Action> _keyActions;
		private string[] _completions;
		private int _completionStart;
		private int _completionsIndex;

		private string _textCache = "";

		public CancellationTokenSource CurrentToken { get; private set; } = null;

		private bool IsColorFormattingEnabled() => _colorFormattingHandler != null;

		private bool IsStartOfLine() => _cursorPos == 0;

		private bool IsEndOfLine() => _cursorPos == _cursorLimit;

		private bool IsStartOfBuffer() => Console.CursorLeft == 0;

		private bool IsEndOfBuffer() => Console.CursorLeft == Console.BufferWidth - 1;
		private bool IsInAutoCompleteMode() => _completions != null;

		private void EmptyAction() { }

		private void MoveCursorLeft() {
			if (IsStartOfLine())
				return;

			if (IsStartOfBuffer())
				Console.SetCursorPosition(Console.BufferWidth - 1, Console.CursorTop - 1);
			else
				Console.SetCursorPosition(Console.CursorLeft - 1, Console.CursorTop);

			_cursorPos--;
		}

		private void MoveCursorLeftWord() {
			int index = _text.ToString().FindPrevWord(_cursorPos);
			while (_cursorPos > index)
				MoveCursorLeft();
		}

		private void MoveCursorHome() {
			while (!IsStartOfLine())
				MoveCursorLeft();
		}

		private string BuildKeyInput() {
			return (_keyInfo.Modifiers != ConsoleModifiers.Control && _keyInfo.Modifiers != ConsoleModifiers.Shift) ?
				_keyInfo.Key.ToString() : _keyInfo.Modifiers.ToString() + _keyInfo.Key.ToString();
		}

		private void MoveCursorRight() {
			if (IsEndOfLine())
				return;

			if (IsEndOfBuffer())
				Console.SetCursorPosition(0, Console.CursorTop + 1);
			else
				Console.SetCursorPosition(Console.CursorLeft + 1, Console.CursorTop);

			_cursorPos++;
		}

		private void MoveCursorRightWord() {
			int index = _text.ToString().FindNextWord(_cursorPos);
			while (_cursorPos < index)
				MoveCursorRight();
		}

		private void MoveCursorEnd() {
			while (!IsEndOfLine())
				MoveCursorRight();
		}

		private void MoveCursorIndex(int index) {
			if (index < 0 || index > _cursorLimit)
				return;
			if (_cursorPos == index)
				return;
			while (_cursorPos < index)
				MoveCursorRight();
			while (_cursorPos > index) {
				MoveCursorLeft();
			}
		}

		private void ClearLine() {
			MoveCursorEnd();
			while (!IsStartOfLine())
				BackspaceInternal();
			if (IsColorFormattingEnabled()) {
				_cfs = new ColorFormattedString();
			}
		}

		private void WriteNewString(string str) {
			ClearLine();
			WriteString(str, false);
			ColorFormat();
		}

		private void WriteString(string str, bool quickColorFormat = true) {
			if (IsEndOfLine()) {
				_cursorLimit += str.Length;
				_text.Append(str);
				Console.Write(str);
				_cursorPos += str.Length;

				if (quickColorFormat && IsColorFormattingEnabled()) {
					var lastComp = _cfs.LastOrDefault();
					var newComp = new ColorFormattedStringComponent(str, lastComp?.Foreground, lastComp?.Background);
					_cfs += newComp;
					QuickColorFormat(_cursorPos - str.Length, new ColorFormattedString(newComp));
				}
			} else if (IsStartOfLine()) {
				_cursorLimit += str.Length;
				int left = Console.CursorLeft;
				int top = Console.CursorTop;
				string after = _text.ToString();
				_text.Insert(0, str);
				Console.Write(str + after);
				Console.SetCursorPosition(left, top);
				int index = str.Length;

				if (quickColorFormat && IsColorFormattingEnabled()) {
					var nextComp = _cfs.FirstOrDefault();
					var newComp = new ColorFormattedStringComponent(str, nextComp?.Foreground, nextComp?.Background);
					_cfs = ColorFormattedString.Empty + newComp + _cfs;

					QuickColorFormat(0, _cfs);
				}
			} else {
				_cursorLimit += str.Length;
				int left = Console.CursorLeft;
				int top = Console.CursorTop;
				string after = _text.ToString()[_cursorPos..];
				_text.Insert(_cursorPos, str);
				Console.Write(str + after);
				Console.SetCursorPosition(left, top);
				int index = _cursorPos + str.Length;

				if (quickColorFormat && IsColorFormattingEnabled()) {
					var cfsLeft =  _cfs.Substring(0, _cursorPos);
					var cfsRight =  _cfs.Substring(_cursorPos);

					var compLeft = cfsLeft.LastOrDefault();
					var compRight = cfsRight.FirstOrDefault();
					ConsoleColor? newFg = null, newBg = null;
					if (compLeft?.Foreground == compRight?.Foreground)
						newFg = compLeft.Foreground;
					if (compLeft?.Background == compRight?.Background)
						newBg = compLeft.Background;
					var compNew = new ColorFormattedStringComponent(str, newFg, newBg);

					_cfs = cfsLeft + compNew + cfsRight;

					QuickColorFormat(index - str.Length, ColorFormattedString.Empty + compNew + cfsRight);
				}

				MoveCursorIndex(index);
			}
		}

		private void ColorFormat(int diffIndex = -1) {
				if (!IsColorFormattingEnabled())
				return;

			Debug.WriteLine(diffIndex);

			var text = _text.ToString();
			var cfs = _colorFormattingHandler.GetColorFormattedString(text);
			if (text != cfs.ToString())
				return;
			if (diffIndex == -1)
				diffIndex = DifferenceCFS(cfs, _cfs);
			if (diffIndex == text.Length)
				return;
			var index = _cursorPos;
			MoveCursorIndex(diffIndex);

			int left = Console.CursorLeft;
			int top = Console.CursorTop;

			var cfsPart = cfs.Substring(diffIndex);

			cfsPart.Write();
			Console.SetCursorPosition(left, top);

			MoveCursorIndex(index);

			_cfs = cfs;

			int DifferenceCFS(ColorFormattedString next, ColorFormattedString prev) {
				var next_e = next.GetEnumerator();
				var prev_e = prev.GetEnumerator();
				int i = 0;
				while (next_e.MoveNext() && prev_e.MoveNext()) {
					var v1 = next_e.Current;
					var v2 = prev_e.Current;
					if (v1 != v2) {
						if (v1.Background == v2.Background && v1.Foreground == v2.Foreground) {
							i += DifferenceString(v1.String, v2.String);
						}
						break;
					}
					i += v1.Length;
				}
				return i;
			}
			int DifferenceString(string next, string prev) {
				int i = 0;
				while (i < next.Length && i < prev.Length) {
					if (next[i] != prev[i])
						break;
					++i;
				}
				return i;
			}
		}

		private void QuickColorFormat(int startIndex, ColorFormattedString cfsPart) {
			if (_colorFormattingHandler == null)
				return;
			var index = _cursorPos;

			MoveCursorIndex(startIndex);

			int left = Console.CursorLeft;
			int top = Console.CursorTop;
			cfsPart.Write();
			Console.SetCursorPosition(left, top);
			MoveCursorIndex(index);
		}

		private void WriteChar() => WriteChar(_keyInfo.KeyChar);

		private void WriteChar(char c, bool quickColorFormat = true) {
			if (c < 32)
				return;

			WriteString(c.ToString(), quickColorFormat);
		}

		private int BackspaceInternal() {
			if (IsStartOfLine())
				return -1;

			MoveCursorLeft();
			int index = _cursorPos;
			_text.Remove(index, 1);
			string replacement = _text.ToString()[index..];
			int left = Console.CursorLeft;
			int top = Console.CursorTop;
			Console.BackgroundColor = ConsoleColor.Black;
			Console.Write($"{replacement} ");
			Console.SetCursorPosition(left, top);
			_cursorLimit--;

			return index;
		}

		private void Backspace() {
			int index = BackspaceInternal();
			if (index < 0)
				return;
			if (IsColorFormattingEnabled()) {
				if (index >= _text.Length - 1) {
					_cfs = _cfs.Substring(0, _cfs.Length - 1);
				} else if (index != -1) {
					var cfsLast = _cfs.Substring(index + 1);
					_cfs = _cfs.Substring(0, index) + cfsLast;

					QuickColorFormat(index, cfsLast);
				}
			}
		}

		private void BackspaceWord() {
			if (IsStartOfLine())
				return;

			int index = _cursorPos;

			var text = _text.ToString()[.._cursorPos];
			var endSpaces = text.CountTrailing(' ');

			int target;
			if (endSpaces > 0) {
				target = text.Length - endSpaces;
			} else {
				target = text.LastIndexOf(' ') + 1;
			}
			while (_cursorPos > target)
				BackspaceInternal();

			if (IsColorFormattingEnabled()) {
				var cfsLast = _cfs.Substring(index);
				_cfs = _cfs.Substring(0, target) + cfsLast;

				QuickColorFormat(target, cfsLast);
			}
		}

		private void Delete() {
			if (IsEndOfLine())
				return;

			int index = _cursorPos;
			_text.Remove(index, 1);

			string replacement = _text.ToString()[index..];
			int left = Console.CursorLeft;
			int top = Console.CursorTop;
			Console.Write($"{replacement} ");
			Console.SetCursorPosition(left, top);
			_cursorLimit--;

			if (IsColorFormattingEnabled()) {
				var cfsLast = _cfs.Substring(index + 1);
				_cfs = _cfs.Substring(0, index) + cfsLast;

				QuickColorFormat(index, cfsLast + " ");
			}
		}

		private void DeleteWord() {

		}

		private void StartAutoComplete() {
			while (_cursorPos > _completionStart)
				BackspaceInternal();

			_completionsIndex = 0;

			WriteString(_completions[_completionsIndex]);

			ColorFormat(_completionStart);
		}

		private void NextAutoComplete() {
			if (_completions.Length == 1)
				return;

			while (_cursorPos > _completionStart)
				BackspaceInternal();

			_completionsIndex++;

			if (_completionsIndex == _completions.Length)
				_completionsIndex = 0;

				WriteString(_completions[_completionsIndex], false);

			if (IsColorFormattingEnabled()) {
				ColorFormat(_completionStart);
			}
		}

		private void PreviousAutoComplete() {
			if (_completions.Length == 1)
				return;
			while (_cursorPos > _completionStart)
				BackspaceInternal();

			_completionsIndex--;

			if (_completionsIndex == -1)
				_completionsIndex = _completions.Length - 1;

			WriteString(_completions[_completionsIndex], false);

			if (IsColorFormattingEnabled()) {
				ColorFormat(_completionStart);
			}
		}

		private void PrevHistory() {
			if (_historyIndex == _history.Count) {
				_textCache = _text.ToString();
			}
			if (_historyIndex > 0) {
				_historyIndex--;
				WriteNewString(_history[_historyIndex]);
			}
		}

		private void NextHistory() {
			if (_historyIndex < _history.Count) {
				_historyIndex++;
				if (_historyIndex == _history.Count)
					WriteNewString(_textCache);
				else
					WriteNewString(_history[_historyIndex]);
			}
		}

		private void ResetAutoComplete() {
			_completions = null;
			_completionsIndex = 0;
		}

		public string Text {
			get {
				return _text.ToString();
			}
		}

		public KeyHandler(List<string> history, IAutoCompleteHandler autoCompleteHandler, IColorFormattingHandler colorFormattingHandler, string placeholder = "") {
			_colorFormattingHandler = colorFormattingHandler;

			_history = history ?? new List<string>();
			_historyIndex = _history.Count;
			_text = new StringBuilder();

			if (IsColorFormattingEnabled()) {
				_cfs = new ColorFormattedString(_text.ToString());
			}

			_keyActions = new Dictionary<string, Action> {
				["LeftArrow"] = MoveCursorLeft,
				["ControlLeftArrow"] = MoveCursorLeftWord,
				["Home"] = MoveCursorHome,
				["End"] = MoveCursorEnd,
				["ControlA"] = MoveCursorHome,
				["ControlB"] = MoveCursorLeft,
				["RightArrow"] = MoveCursorRight,
				["ControlRightArrow"] = MoveCursorRightWord,
				["ControlF"] = MoveCursorRight,
				["ControlE"] = MoveCursorEnd,
				["Backspace"] = Backspace,
				["ShiftBackspace"] = Backspace,
				["ControlBackspace"] = BackspaceWord,
				["Delete"] = Delete,
				["ShiftDelete"] = Delete,
				["ControlDelete"] = EmptyAction,
				["ControlD"] = Delete,
				["ControlH"] = Backspace,
				["ControlL"] = ClearLine,
				["Escape"] = ClearLine,
				["UpArrow"] = PrevHistory,
				["ControlP"] = PrevHistory,
				["DownArrow"] = NextHistory,
				["ControlN"] = NextHistory,
				["ControlU"] = () => {
					int index = _cursorPos;
					while (!IsStartOfLine())
						BackspaceInternal();

					if (IsColorFormattingEnabled()) {
						_cfs = _cfs.Substring(index);

						QuickColorFormat(_cursorPos, _cfs);
					}

				},
				["ControlK"] = () => {
					int pos = _cursorPos;
					MoveCursorEnd();
					while (_cursorPos > pos)
						BackspaceInternal();

					if (IsColorFormattingEnabled()) {
						_cfs = _cfs.Substring(0, pos);
					}
				},

				["Tab"] = () => {
					if (IsInAutoCompleteMode()) {
						NextAutoComplete();
					} else {
						if (autoCompleteHandler == null)
							return;

						string text = _text.ToString()[.._cursorPos];

						_completionStart = text.LastIndexOfAny(autoCompleteHandler.Separators.ToArray()) + 1;

						_completions = autoCompleteHandler.GetSuggestions(text, _completionStart).ToArray();
						_completions = _completions?.Length == 0 ? null : _completions;

						if (_completions == null)
							return;

						StartAutoComplete();
					}
				},

				["ShiftTab"] = () => {
					if (IsInAutoCompleteMode()) {
						PreviousAutoComplete();
					}
				}
			};

			if (!String.IsNullOrEmpty(placeholder)) {
				WriteNewString(placeholder);
			}
		}

		public void Handle(ConsoleKeyInfo keyInfo) {
			Console.CursorVisible = false;
			_keyInfo = keyInfo;

			// If in auto complete mode and Tab wasn't pressed
			if (IsInAutoCompleteMode() && _keyInfo.Key != ConsoleKey.Tab)
				ResetAutoComplete();

			_keyActions.TryGetValue(BuildKeyInput(), out Action action);
			action ??= WriteChar;
			action.Invoke();

			CurrentToken?.Cancel();
			CurrentToken = new CancellationTokenSource();
			Task.Delay(750, CurrentToken.Token).ContinueWith(task => {
				if (!task.IsCanceled) {
					Console.CursorVisible = false;
					ColorFormat();
					Console.CursorVisible = true;
				}
			});

			Console.CursorVisible = true;
		}
	}
}
