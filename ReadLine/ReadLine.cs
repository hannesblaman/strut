﻿using Internal.ReadLine;

using System.Collections.Generic;

namespace System {
	public static class ReadLine {
		private static List<string> _history;

		static ReadLine() {
			_history = new List<string>();
		}

		public static void AddHistory(params string[] text) => _history.AddRange(text);
		public static List<string> GetHistory() => _history;
		public static void ClearHistory() => _history = new List<string>();
		public static bool HistoryEnabled { get; set; }
		public static IAutoCompleteHandler AutoCompletionHandler { private get; set; }
		public static IColorFormattingHandler ColorFormattingHandler { private get; set; }

		public static string Read(string prompt = "", string @default = "") {
			Console.CursorVisible = true;
			Console.Write(prompt);
			KeyHandler keyHandler = new KeyHandler(_history, AutoCompletionHandler, ColorFormattingHandler, @default);
			string text = GetText(keyHandler);

			if (text == null) {
				return null;
			}

			if (String.IsNullOrWhiteSpace(text) && !String.IsNullOrWhiteSpace(@default)) {
				text = @default;
			} else {
				if (HistoryEnabled)
					_history.Add(text);
			}

			Console.CursorVisible = false;
			return text;
		}

		private static string GetText(KeyHandler keyHandler) {
			ConsoleKeyInfo keyInfo;
			string output = null;
			while (true) {
				keyInfo = Console.ReadKey(true);

				switch (keyInfo.Key) {
					case ConsoleKey.Enter:
						output = keyHandler.Text;
						goto esc;
					case ConsoleKey.Escape:
						goto esc;
					default:
						keyHandler.Handle(keyInfo);
						break;
				}

			}
		esc:
			keyHandler.CurrentToken?.Cancel();
			return output;
		}
	}
}
